/*
 * Copyright (c) 2016
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.extropy.quiz.common.bo;

import java.util.List;

/**
 * User: pratyushr
 * Date: 1/30/16
 * Time: 9:42 AM
 */
public class QuestionBo {
    private Long id;

    private String questionText;

    private String language;

    private AnswerBo rightAnswer;

    private List<AnswerBo> wrongAnswers;

    private Long subjectId;

    private Integer level;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public AnswerBo getRightAnswer() {
        return rightAnswer;
    }

    public void setRightAnswer(AnswerBo rightAnswer) {
        this.rightAnswer = rightAnswer;
    }

    public List<AnswerBo> getWrongAnswers() {
        return wrongAnswers;
    }

    public void setWrongAnswers(List<AnswerBo> wrongAnswers) {
        this.wrongAnswers = wrongAnswers;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }


    @Override
    public String toString() {
        return "QuestionBo{" +
                "id=" + id +
                ", questionText='" + questionText + '\'' +
                ", language='" + language + '\'' +
                ", rightAnswer=" + rightAnswer +
                ", wrongAnswers=" + wrongAnswers +
                ", subjectId=" + subjectId +
                ", level=" + level +
                '}';
    }
}

