package com.extropy.quiz.common.annotation;

import com.extropy.app.exception.ProcessingException;
import com.extropy.app.exception.ValidationException;
import org.springframework.transaction.annotation.Transactional;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * User: pratyushr
 * Date: 2/7/16
 * Time: 10:45 AM
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Transactional(rollbackFor = {RuntimeException.class, ProcessingException.class,  ValidationException.class})

public @interface QuizTransactional {
}
