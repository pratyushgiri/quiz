/*
 * Copyright (c) 2016
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.extropy.quiz.dal.service;

import com.extropy.quiz.common.bo.QuestionBo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * User: pratyushr
 * Date: 1/30/16
 * Time: 3:38 PM
 */
@Service
public interface QuestionDbService {

    /**
     * This is a test method. needs to be removed.
     * @return
     */
    //TODO remove this.
    @Deprecated
    List<QuestionBo> getQuestions();

    List<QuestionBo> saveQuestions( List<QuestionBo> Bos);

    QuestionBo saveQuestion(QuestionBo bo);


}
