/*
 * Copyright (c) 2016
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.extropy.quiz.dal.model;


import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * User: pratyushr
 * Date: 1/30/16
 * Time: 7:29 AM
 */
@Table(name = "Question")
@Entity
public class Question {
    private static final long serialVersionId = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique=true ,nullable = false, updatable=false)
    private Long id;

    @Column(name ="question_text", length = 4000, nullable = false)
    private String questionText;

    @Column(name ="lang", length = 2)
    private String language;

    @Column(name ="correct_ans")
    private String correctAnswer;

    @Column(name ="wrong_ans1")
    private String wrongAnswer1;

    @Column(name ="wrong_ans2")
    private String wrongAnswer2;

    @Column(name ="wrong_ans3")
    private String wrongAnswer3;

    @Column(name ="wrong_ans4")
    private String wrongAnswer4;

    @Column(name ="level")
    private Integer level;

    @Column(name ="subject_id")
    private Long subjectId;


    @Column(name="validated_flag", columnDefinition = "TINYINT")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    public boolean isValidated;




    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public String getWrongAnswer1() {
        return wrongAnswer1;
    }

    public void setWrongAnswer1(String wrongAnswer1) {
        this.wrongAnswer1 = wrongAnswer1;
    }

    public String getWrongAnswer2() {
        return wrongAnswer2;
    }

    public void setWrongAnswer2(String wrongAnswer2) {
        this.wrongAnswer2 = wrongAnswer2;
    }

    public String getWrongAnswer3() {
        return wrongAnswer3;
    }

    public void setWrongAnswer3(String wrongAnswer3) {
        this.wrongAnswer3 = wrongAnswer3;
    }

    public String getWrongAnswer4() {
        return wrongAnswer4;
    }

    public void setWrongAnswer4(String wrongAnswer4) {
        this.wrongAnswer4 = wrongAnswer4;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public boolean isVlidated() {
        return isValidated;
    }

    public void setVlidated(boolean vlidated) {
        isValidated = vlidated;
    }


    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", questionText='" + questionText + '\'' +
                ", language='" + language + '\'' +
                ", correctAnswer='" + correctAnswer + '\'' +
                ", wrongAnswer1='" + wrongAnswer1 + '\'' +
                ", wrongAnswer2='" + wrongAnswer2 + '\'' +
                ", wrongAnswer3='" + wrongAnswer3 + '\'' +
                ", wrongAnswer4='" + wrongAnswer4 + '\'' +
                ", level=" + level +
                ", subjectId=" + subjectId +
                ", isValidated=" + isValidated +
                '}';
    }
}
