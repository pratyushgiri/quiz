/*
 * Copyright (c) 2016
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.extropy.quiz.dal.transformers;

import com.extropy.quiz.common.bo.SubjectBo;
import com.extropy.quiz.dal.model.Subject;

/**
 * User: pratyushr
 * Date: 2/2/16
 * Time: 11:07 PM
 */
public class SubjectTransformer {

    public SubjectBo toSubjectBo(Subject subject) {

        if (subject != null){
            SubjectBo bo = new SubjectBo();
            bo.setId(subject.getId());
            bo.setSubjectName(subject.getName());
            return bo;
        }
        return null;
    }

    public Subject toSubject(SubjectBo bo) {
        if (bo != null) {
            Subject s = new Subject();
            s.setId(bo.getId());
            s.setName(bo.getSubjectName());
            return s;
        }
        return null;
    }

}
