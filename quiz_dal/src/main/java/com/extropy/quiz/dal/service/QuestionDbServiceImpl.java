/*
 * Copyright (c) 2016
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.extropy.quiz.dal.service;

import com.extropy.quiz.common.annotation.QuizTransactional;
import com.extropy.quiz.common.bo.QuestionBo;
import com.extropy.quiz.dal.model.Question;
import com.extropy.quiz.dal.repository.QuestionRepository;
import com.extropy.quiz.dal.transformers.QuestionTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * User: pratyushr
 * Date: 1/30/16
 * Time: 8:09 AM
 */
@Service
public class QuestionDbServiceImpl implements QuestionDbService{

    private static final Logger logger =
                LoggerFactory.getLogger(QuestionDbServiceImpl.class);

    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    QuestionTransformer questionTransformer;

    public List<QuestionBo> getQuestions() {

        logger.info("Getting all questions.");

        List<Question> questions = questionRepository.getAllQuestions();

        List<QuestionBo> questionBos = questionTransformer.toQuestionBoSet(questions);

        logger.info("Found {}  questions.", questionBos.size());

        return questionBos;

    }

    @QuizTransactional
    public List<QuestionBo> saveQuestions(List<QuestionBo> questionBos) {

        logger.info("Saving Questions.");

        List<QuestionBo> returnValue = new ArrayList<QuestionBo>();

        for (QuestionBo bo : questionBos){

            Question q = questionTransformer.toQuestion(bo);
            q = questionRepository.save(q);
            returnValue.add(questionTransformer.toQuestionBo(q));
        }

        logger.info("Saved Questions.");
        return returnValue;
    }

    @QuizTransactional
    public QuestionBo saveQuestion(QuestionBo bo) {

        logger.info("Saving Question.");
        Question q = questionTransformer.toQuestion(bo);
        q = questionRepository.save(q);
        logger.info("Saved Question.");
        return questionTransformer.toQuestionBo(q);

    }



}
