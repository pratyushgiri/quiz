/*
 * Copyright (c) 2016
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.extropy.quiz.dal.service;

import com.extropy.quiz.common.annotation.QuizTransactional;
import com.extropy.quiz.common.bo.SubjectBo;
import com.extropy.quiz.dal.model.Subject;
import com.extropy.quiz.dal.repository.SubjectRepository;
import com.extropy.quiz.dal.transformers.SubjectTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * User: pratyushr
 * Date: 2/2/16
 * Time: 11:11 PM
 */
@Service
public class SubjectDbServiceImpl implements SubjectDbService{
    private static final Logger logger =
                   LoggerFactory.getLogger(SubjectDbServiceImpl.class);

    @Autowired
    SubjectRepository subjectRepository;

    @Autowired
    SubjectTransformer subjectTransformer;

    public List<SubjectBo> getAllSubjects() {

        logger.info("Getting All Subjects");

        List<Subject> subjects = subjectRepository.getAllSubjects();

        List<SubjectBo> returnValue  =  new ArrayList<SubjectBo>() ;

        for (Subject subject : subjects) {
            returnValue.add(subjectTransformer.toSubjectBo(subject));
        }

        logger.info("Got all Subjects");

        return returnValue;
    }

    public SubjectBo getSubject(String subjectName) {

        logger.info("Getting  Subject for {}", subjectName);

        SubjectBo bo = null;

        if (!StringUtils.isEmpty(subjectName)) {
            bo = subjectTransformer.toSubjectBo(subjectRepository.getSubject(subjectName));
            logger.info("Found Subject  {}", subjectName);
        }
        return bo;
    }

    @QuizTransactional
    public SubjectBo saveSubject(SubjectBo subjectBo) {
        logger.info("Saving Subject {}", subjectBo);
        if (subjectBo != null) {
            Subject subject =  subjectTransformer.toSubject(subjectBo) ;
            subject = subjectRepository.save(subject);
            return subjectTransformer.toSubjectBo(subject) ;
        }

        return null;
    }


}
