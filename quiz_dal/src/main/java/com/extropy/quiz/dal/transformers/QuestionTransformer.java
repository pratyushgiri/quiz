/*
 * Copyright (c) 2016
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.extropy.quiz.dal.transformers;

import com.extropy.quiz.common.bo.AnswerBo;
import com.extropy.quiz.common.bo.QuestionBo;
import com.extropy.quiz.dal.model.Question;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * User: pratyushr
 * Date: 1/30/16
 * Time: 3:43 PM
 */

@Component
public class QuestionTransformer {

    private static final Logger logger =
            LoggerFactory.getLogger(QuestionTransformer.class);

    /**
     * Transforms a List of Question Entities to QuestionBos
     * @param questionList
     * @return
     */
    public List<QuestionBo> toQuestionBoSet(List<Question> questionList) {

        List<QuestionBo> bos = new ArrayList();

        for (Question q : questionList){
            bos.add(toQuestionBo(q));
        }
        return bos;
    }

    /**
     * Transforms a Question Entity to QuestionBo
     * @param question
     * @return
     */
    public QuestionBo toQuestionBo(Question question) {

        logger.debug("Transforming Question Entity: {}", question);

        QuestionBo bo = new QuestionBo();
        bo.setId(question.getId());
        bo.setLanguage(question.getLanguage());
        bo.setQuestionText(question.getQuestionText());
        bo.setRightAnswer(getCorrectAnswer(question));
        bo.setWrongAnswers(getWrongAnswers(question));
        bo.setLevel(question.getLevel());
        bo.setSubjectId(question.getSubjectId());


        logger.debug("Transformed to Question BO : {}", bo);
        return bo;
    }

    /**
     * Transforms a Question Entity from a Question BO.
     * @param bo
     * @return
     */
    public Question toQuestion (QuestionBo bo){
        Question q = null;
        if (bo != null){
            q = new Question();
            //set right answer
            if (!StringUtils.isEmpty(bo.getRightAnswer())) {
                q.setCorrectAnswer(bo.getRightAnswer().getAnswerText());
            }
            //set wrong answers
            setWrongAnswers(bo, q);
            //set others
            q.setLanguage(bo.getLanguage());
            q.setLevel(bo.getLevel());
            q.setQuestionText(bo.getQuestionText());
            q.setSubjectId(bo.getSubjectId());
            q.setVlidated(true);
        }
        return q;
    }

    /**
     *
     * @param bo
     * @param q
     */
    private void setWrongAnswers(QuestionBo bo, Question q) {
        List<AnswerBo> wrongAnswers = bo.getWrongAnswers();
        //TODO we will fix it later
        if (wrongAnswers != null && !wrongAnswers.isEmpty()) {
            int size =  wrongAnswers.size();
            if (size >= 4) {
                q.setWrongAnswer1(wrongAnswers.get(0).getAnswerText());
                q.setWrongAnswer2(wrongAnswers.get(1).getAnswerText());
                q.setWrongAnswer3(wrongAnswers.get(2).getAnswerText());
                q.setWrongAnswer4(wrongAnswers.get(3).getAnswerText());
            } else if (size == 3) {
                q.setWrongAnswer1(wrongAnswers.get(0).getAnswerText());
                q.setWrongAnswer2(wrongAnswers.get(1).getAnswerText());
                q.setWrongAnswer3(wrongAnswers.get(2).getAnswerText());
            } else if (size == 2) {
                q.setWrongAnswer1(wrongAnswers.get(0).getAnswerText());
                q.setWrongAnswer2(wrongAnswers.get(1).getAnswerText());
            } else if (size == 1) {
                q.setWrongAnswer1(wrongAnswers.get(0).getAnswerText());
            }
        }
    }

    /**
     * Sets the wrong answers for a question
     * @param question
     * @return
     */
    private List<AnswerBo> getWrongAnswers(Question question) {
        AnswerBo wrongAnswer1  = new AnswerBo();
        wrongAnswer1.setAnswerText(question.getWrongAnswer1());

        AnswerBo wrongAnswer2  = new AnswerBo();
        wrongAnswer2.setAnswerText(question.getWrongAnswer2());

        AnswerBo wrongAnswer3  = new AnswerBo();
        wrongAnswer3.setAnswerText(question.getWrongAnswer3());

        AnswerBo wrongAnswer4  = new AnswerBo();
        wrongAnswer4.setAnswerText(question.getWrongAnswer4());

        List<AnswerBo> wrongAnswers = new ArrayList<AnswerBo>();
        wrongAnswers.add(wrongAnswer1);
        wrongAnswers.add(wrongAnswer2);
        wrongAnswers.add(wrongAnswer3);
        wrongAnswers.add(wrongAnswer4);

        return wrongAnswers;
    }

    /**
     * Sets the right answer for a question
     * @param question
     * @return
     */
    private AnswerBo getCorrectAnswer(Question question) {
        AnswerBo correctAnswer = new AnswerBo();
        correctAnswer.setAnswerText(question.getCorrectAnswer());
        return correctAnswer;
    }
    
    

}
