/*
 * Copyright (c) 2016
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.extropy.quiz.dal.repository;

import com.extropy.quiz.dal.model.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

/**
 * User: pratyushr
 * Date: 2/2/16
 * Time: 10:57 PM
 */
@Repository
public interface SubjectRepository extends JpaRepository<Subject, BigInteger> {
    @Query("select s from Subject s")
    public List<Subject> getAllSubjects();

    @Query ("select s from Subject s where name = ?1")
    public Subject getSubject(String subjectName);

}
