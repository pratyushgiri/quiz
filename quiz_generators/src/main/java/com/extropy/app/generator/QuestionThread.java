/*
 * Copyright 2016.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.extropy.app.generator;

import com.extropy.app.enums.SubjectEnum;
import com.extropy.app.exception.ProcessingException;
import com.extropy.quiz.business.pojo.QuestionRequest;
import com.extropy.quiz.business.service.QuestionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.Random;

/**
 * User: pratyushr
 * Date: 2/8/16
 * Time: 10:00 PM
 */

public class QuestionThread  extends Thread {

    private static final Logger logger =
                LoggerFactory.getLogger(QuestionThread.class);

    private String name;
    private SubjectEnum subjectName;
    private QuestionService questionService;
    private int questionCount;

    private QuestionThread(){}

    public QuestionThread(String name, SubjectEnum subjectName, QuestionService questionService, int countOfQuestion) {
        this.name = name;
        this.questionService = questionService;
        this.subjectName = subjectName;
        this.questionCount = countOfQuestion;
    }


    public void run() {

        logger.info("Creating questions in thread {}", name);

        for (int i=0; i < questionCount; i++){
            int level = randomLevel(1,100);
            QuestionRequest request = getARequest(subjectName, name, i, level, "en" );
            try {
                questionService.addQuestion(request) ;
            } catch (ProcessingException e) {
                logger.error("Error in Thread{}  for count {}", new Object[]{name,i});
                e.printStackTrace();
            }
            logger.info("Created question {} using thread {}", new Object[]{i, name});
        }
        logger.info("Finished creating questions in thread {}", name);

    }

    /**
     * Not really needed to be secured.
     * @param low
     * @param high
     * @return
     */
    private int randomLevel(int low, int high) {
        Random r = new Random();
        return (r.nextInt(high-low) + low);
    }


    private QuestionRequest getARequest(SubjectEnum subjectEnum,
                                            String threadName,
                                            int count,
                                            int level,
                                            String lang) {
        QuestionRequest request = new QuestionRequest();

        String subjectName = subjectEnum.name();
        String identifier = subjectName + ":" + threadName + ":"+ count;
        String questionText = "QuestionText->"+identifier;
        String correctAnswer =  "CorrectAnswer->"+identifier;
        String wrongAnswer1 =  "Wrong1->"+identifier;
        String wrongAnswer2 =  "Wrong2->"+identifier;
        String wrongAnswer3 =  "Wrong3->"+identifier;
        String wrongAnswer4 =  "Wrong4->"+identifier;

        request.setSubjectName(subjectName);
        request.setLevel(level);
        request.setQuestionText(questionText);
        request.setCorrectAnswer(correctAnswer);
        request.setWrongAnswer1(wrongAnswer1);
        request.setWrongAnswer2(wrongAnswer2);
        request.setWrongAnswer3(wrongAnswer3);
        request.setWrongAnswer4(wrongAnswer4);
        request.setLanguage(lang);

        return request;
    }


}
