/*
 * Copyright (c) 2016
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.extropy.app.generator;


import com.extropy.app.enums.SubjectEnum;
import com.extropy.app.exception.ProcessingException;
import com.extropy.quiz.business.pojo.QuestionRequest;
import com.extropy.quiz.business.service.QuestionService;
import com.extropy.quiz.common.bo.QuestionBo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;


/**
 * User: pratyushr
 * Date: 2/4/16
 * Time: 7:03 PM
 */

@Component
public class QuestionsGenerator  {

    private static final Logger logger =
            LoggerFactory.getLogger(QuestionsGenerator.class);

    ThreadPoolExecutor executor;

    public static void main (String[] args) throws ProcessingException {

        ApplicationContext applicationContext = new ClassPathXmlApplicationContext
                                                    ("classpath:spring-quiz-generator.xml");

        QuestionsGenerator questionsGenerator = applicationContext.getBean(QuestionsGenerator.class);


        long startTime = System.currentTimeMillis();

        int questionCount = 10000;

       // questionsGenerator.generateQuestions(questionCount);
        questionsGenerator.generateOneQuestion();

        int totalQuestions = questionCount * 10;

        long estimatedTime = System.currentTimeMillis() - startTime;

        logger.info(" Time took to generate {} questions {} MS", new Object[]{questionCount, estimatedTime});


    }

    private void generateOneQuestion() throws ProcessingException {
        QuestionRequest qq = getARequest(SubjectEnum.SUBJECT_1,"PRATYUSH",1,11,"en")  ;
        QuestionBo bb = questionService.addQuestion(qq);
        System.out.println("DONE...") ;
    }

    private void generateQuestions(int countOfQuestions) {
        logger.info("Starting Generating Questions");

        executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);

        Thread taskSubject1 =  new QuestionThread("T1", SubjectEnum.SUBJECT_1, questionService, countOfQuestions);
        Thread taskSubject2 =  new QuestionThread("T2", SubjectEnum.SUBJECT_2, questionService, countOfQuestions);
        Thread taskSubject3 =  new QuestionThread("T3", SubjectEnum.SUBJECT_3, questionService, countOfQuestions);
        Thread taskSubject4 =  new QuestionThread("T4", SubjectEnum.SUBJECT_4, questionService, countOfQuestions);
        Thread taskSubject5 =  new QuestionThread("T5", SubjectEnum.SUBJECT_5, questionService, countOfQuestions);
        Thread taskSubject6 =  new QuestionThread("T6", SubjectEnum.SUBJECT_6, questionService, countOfQuestions);
        Thread taskSubject7 =  new QuestionThread("T7", SubjectEnum.SUBJECT_7, questionService, countOfQuestions);
        Thread taskSubject8 =  new QuestionThread("T8", SubjectEnum.SUBJECT_8, questionService, countOfQuestions);
        Thread taskSubject9 =  new QuestionThread("T9", SubjectEnum.SUBJECT_9, questionService, countOfQuestions);
        Thread taskSubject10 =  new QuestionThread("T10", SubjectEnum.SUBJECT_10, questionService, countOfQuestions);

        executor.submit(taskSubject1);
        executor.submit(taskSubject2);
        executor.submit(taskSubject3);
        executor.submit(taskSubject4);
        executor.submit(taskSubject5);
        executor.submit(taskSubject6);
        executor.submit(taskSubject7);
        executor.submit(taskSubject8);
        executor.submit(taskSubject9);
        executor.submit(taskSubject10);

        logger.info("Trying to Shutdown Executor");



        executor.shutdown();


        logger.info("Finished question Generation");

    }


    private QuestionRequest getARequest(SubjectEnum subjectEnum,
                                                String threadName,
                                                int count,
                                                int level,
                                                String lang) {
            QuestionRequest request = new QuestionRequest();

            String subjectName = subjectEnum.name();
            String identifier = subjectName + ":" + threadName + ":"+ count;
            String questionText = "QuestionText->"+identifier;
            String correctAnswer =  "CorrectAnswer->"+identifier;
            String wrongAnswer1 =  "Wrong1->"+identifier;
            String wrongAnswer2 =  "Wrong2->"+identifier;
            String wrongAnswer3 =  "Wrong3->"+identifier;
            String wrongAnswer4 =  "Wrong4->"+identifier;

            request.setSubjectName(subjectName);
            request.setLevel(level);
            request.setQuestionText(questionText);
            request.setCorrectAnswer(correctAnswer);
            request.setWrongAnswer1(wrongAnswer1);
            request.setWrongAnswer2(wrongAnswer2);
            request.setWrongAnswer3(wrongAnswer3);
            request.setWrongAnswer4(wrongAnswer4);
            request.setLanguage(lang);

            return request;
        }





    @Autowired
    private QuestionService questionService;


}
