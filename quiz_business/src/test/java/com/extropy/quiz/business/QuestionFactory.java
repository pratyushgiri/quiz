package com.extropy.quiz.business;

import com.extropy.quiz.business.pojo.QuestionRequest;
import com.extropy.quiz.common.bo.AnswerBo;
import com.extropy.quiz.common.bo.QuestionBo;

import java.util.ArrayList;

/**
 * User: pratyushr
 * Date: 2/9/16
 * Time: 2:44 PM
 */
public class QuestionFactory {


    static QuestionRequest  createQuestionWithNullQuestionTest() {
        return createQuestion("en", null,
                "right","subjectName",
                "wrong1", "wrong2","wrong3","wrong4");
    }

    static QuestionRequest  createQuestionWithEmptyQuestionText() {
          return createQuestion("en", "",
                  "right","subjectName",
                  "wrong1", "wrong2","wrong3","wrong4");
    }

    static QuestionRequest createQuestionNullRightAnswer(){
        return createQuestion("en", "questionText",
                          null,"subjectName",
                          "wrong1", "wrong2","wrong3","wrong4");
    }

    static QuestionRequest createQuestionEmptyRightAnswer(){
        return createQuestion("en", "questionText",
                          "","subjectName",
                          "wrong1", "wrong2","wrong3","wrong4");
    }

    static QuestionRequest createQuestionNullWrongAnswers(){
            return createQuestion("en", "questionText",
                              "right","subjectName",
                              null, null,null,null);
    }

    static QuestionRequest createQuestionEmptyWrongAnswers(){
                return createQuestion("en", "questionText",
                                  "right","subjectName",
                                  "", "","","");
    }

    static QuestionRequest createAGoodQuestion(){
                    return createQuestion("en", "questionText",
                                      "right","subjectName",
                                      "wrong1", "wrong2","wrong3","wrong4");
    }





    private static QuestionRequest createQuestion(String lang,
                                          String qText,
                                          String rAnswer,
                                          String subjectName,
                                          String wAnswer1,
                                          String wAnswer2,
                                          String wAnswer3,
                                          String wAnswer4){
        QuestionRequest q = new QuestionRequest();
        q.setLanguage(lang);
        q.setSubjectName(subjectName);
        q.setQuestionText(qText);
        q.setCorrectAnswer(rAnswer);
        q.setWrongAnswer1(wAnswer1);
        q.setWrongAnswer2(wAnswer2);
        q.setWrongAnswer3(wAnswer3);
        q.setWrongAnswer4(wAnswer4);
        q.setLevel(20);

        return q;
    }

    static QuestionBo createQuestionBo(QuestionRequest aGoodRequest) {
            QuestionBo bo = new QuestionBo();
        bo.setId(1L);
        bo.setLanguage(aGoodRequest.getLanguage());
        bo.setLevel(aGoodRequest.getLevel());
        bo.setQuestionText(aGoodRequest.getQuestionText());
        bo.setWrongAnswers(new ArrayList<AnswerBo>());

        AnswerBo ansBo = new AnswerBo();
        ansBo.setAnswerText(aGoodRequest.getCorrectAnswer());
        bo.setRightAnswer(ansBo);

        AnswerBo wrongAns1 = new AnswerBo();
        wrongAns1.setAnswerText(aGoodRequest.getWrongAnswer1());
        bo.getWrongAnswers().add(wrongAns1);

        AnswerBo wrongAns2 = new AnswerBo();
        wrongAns2.setAnswerText(aGoodRequest.getWrongAnswer2());
        bo.getWrongAnswers().add(wrongAns2);

        AnswerBo wrongAns3 = new AnswerBo();
        wrongAns3.setAnswerText(aGoodRequest.getWrongAnswer3());
        bo.getWrongAnswers().add(wrongAns3);

        AnswerBo wrongAns4 = new AnswerBo();
        wrongAns4.setAnswerText(aGoodRequest.getWrongAnswer4());
        bo.getWrongAnswers().add(wrongAns4);

        bo.setSubjectId(1L);

        return bo;

    }
}
