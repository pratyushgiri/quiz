package com.extropy.quiz.business;


import com.extropy.app.exception.ProcessingException;
import com.extropy.app.exception.ValidationException;
import com.extropy.quiz.business.pojo.QuestionRequest;
import com.extropy.quiz.business.service.QuestionServiceImpl;
import com.extropy.quiz.common.bo.QuestionBo;
import com.extropy.quiz.common.bo.SubjectBo;
import com.extropy.quiz.dal.service.QuestionDbService;
import com.extropy.quiz.dal.service.SubjectDbService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.testng.Assert.*;

/**
 * User: pratyushr
 * Date: 2/9/16
 * Time: 11:50 AM
 */
public class QuestionServiceUnitTest {

    @InjectMocks
    QuestionServiceImpl service = new QuestionServiceImpl();

    @Mock
    QuestionDbService questionDbService;

    @Mock
    SubjectDbService subjectDbService;


    @BeforeMethod
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test (description = "calling add a question with a null request")
    public void testAddQuestionNull() {
        try {
            service.addQuestion(null);
            fail("Should have thrown an exception");
        } catch (ProcessingException e){
           if ( !(e instanceof ValidationException)) {
               fail("Should have thrown an exception of type ValidationException");
           }
        }
    }

    @Test (description = "calling add a question with a null question text")
    public void testAddQuestionNullText() {
        try {
            service.addQuestion(QuestionFactory.createQuestionWithNullQuestionTest());
            fail("Should have thrown an exception");
        } catch (ProcessingException e){
           if ( !(e instanceof ValidationException)) {
               fail("Should have thrown an exception of type ValidationException");
           }
        }
    }

    @Test (description = "calling add a question with a empty question text")
    public void testAddQuestionEmptyText() {
        try {
            service.addQuestion(QuestionFactory.createQuestionWithEmptyQuestionText());
            fail("Should have thrown an exception");
        } catch (ProcessingException e){
           if ( !(e instanceof ValidationException)) {
               fail("Should have thrown an exception of type ValidationException");
           }
        }
    }
    @Test (description = "calling add a question with a null right answer")
    public void testAddQuestionNullRightAnswer() {
        try {
            service.addQuestion(QuestionFactory.createQuestionNullRightAnswer());
            fail("Should have thrown an exception");
        } catch (ProcessingException e){
           if ( !(e instanceof ValidationException)) {
               fail("Should have thrown an exception of type ValidationException");
           }
        }
    }

    @Test (description = "calling add a question with a empty right answer")
    public void testAddQuestionEmptyRightAnswer() {
        try {
            service.addQuestion(QuestionFactory.createQuestionEmptyRightAnswer());
            fail("Should have thrown an exception");
        } catch (ProcessingException e){
           if ( !(e instanceof ValidationException)) {
               fail("Should have thrown an exception of type ValidationException");
           }
        }
    }

    @Test (description = "calling add a question with a null wrong answers")
    public void testAddQuestionNullWrongAnswers() {
        try {
            service.addQuestion(QuestionFactory.createQuestionNullWrongAnswers());
            fail("Should have thrown an exception");
        } catch (ProcessingException e){
           if ( !(e instanceof ValidationException)) {
               fail("Should have thrown an exception of type ValidationException");
           }
        }
    }

    @Test (description = "calling add a question with a empty wrong answers")
    public void testAddQuestionEmptyWrongAnswers() {
        try {
            service.addQuestion(QuestionFactory.createQuestionEmptyWrongAnswers());
            fail("Should have thrown an exception");
        } catch (ProcessingException e){
           if ( !(e instanceof ValidationException)) {
               fail("Should have thrown an exception of type ValidationException");
           }
        }
    }

    @Test (description = "Add a good question")
    public void testAddQuestionSuccess() {

        QuestionRequest aGoodRequest = QuestionFactory.createAGoodQuestion();

        assertNotNull(aGoodRequest);

        when(subjectDbService.getSubject(anyString())).thenReturn(null);

        when(subjectDbService.saveSubject(any(SubjectBo.class))).thenReturn(SubjectFactory.createSubjectBo());

        when(questionDbService.saveQuestion(any(QuestionBo.class))).thenReturn(QuestionFactory.createQuestionBo(aGoodRequest));


        try {

            QuestionBo bo = service.addQuestion(aGoodRequest);

            assertNotNull(bo);

            assertEquals(aGoodRequest.getCorrectAnswer(), bo.getRightAnswer().getAnswerText());

        } catch (ProcessingException e) {
            fail("Failed to add a question: " + e.getMessage());
        }
    }


}
