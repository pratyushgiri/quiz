package com.extropy.quiz.business;

import com.extropy.quiz.business.transformer.OptimusPrime;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertNull;

/**
 * User: pratyushr
 * Date: 2/11/16
 * Time: 9:30 PM
 */
public class OptimusPrimeUnitTest {
    @InjectMocks
    OptimusPrime prime = new OptimusPrime();


    @BeforeMethod
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testNulltoQuestionBo(){

        assertNull(prime.toQuestionBo(null)) ;

    }



}
