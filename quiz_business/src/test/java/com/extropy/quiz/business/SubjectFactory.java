package com.extropy.quiz.business;

import com.extropy.quiz.common.bo.SubjectBo;

/**
 * User: pratyushr
 * Date: 2/9/16
 * Time: 3:55 PM
 */
public class SubjectFactory {


    static SubjectBo createSubjectBo(){
        SubjectBo bo = new SubjectBo();
        bo.setId(1L);
        bo.setSubjectName("someSubject");

        return bo;
    }
}
