/*
 * Copyright (c) 2016
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.extropy.quiz.business.transformer;

import com.extropy.quiz.business.pojo.QuestionRequest;
import com.extropy.quiz.common.AbstractConstants;
import com.extropy.quiz.common.bo.AnswerBo;
import com.extropy.quiz.common.bo.QuestionBo;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * User: pratyushr
 * Date: 2/3/16
 * Time: 12:18 AM
 */
@Component
public class OptimusPrime {


    public QuestionBo toQuestionBo(QuestionRequest request) {

        if (request ==null){
            return null;
        }

        QuestionBo bo = new QuestionBo();
        bo.setLanguage(StringUtils.isEmpty(request.getLanguage()) ? AbstractConstants.DEFAULT_LANGUAGE
                : request.getLanguage());

        bo.setLevel(request.getLevel());

        bo.setQuestionText(request.getQuestionText());
        bo.setRightAnswer(getAnswer(request.getCorrectAnswer()));
        bo.setWrongAnswers(getWrongAnswer(request));


        return   bo;
    }

    private List<AnswerBo> getWrongAnswer(QuestionRequest request) {

        List<AnswerBo> returnValue = new ArrayList();

        if (!StringUtils.isEmpty(request.getWrongAnswer1())) {
            returnValue.add(getAnswer(request.getWrongAnswer1()));
        }

        if (!StringUtils.isEmpty(request.getWrongAnswer2())) {
            returnValue.add(getAnswer(request.getWrongAnswer2()));
        }

        if (!StringUtils.isEmpty(request.getWrongAnswer3())) {
            returnValue.add(getAnswer(request.getWrongAnswer3()));
        }

        if (!StringUtils.isEmpty(request.getWrongAnswer4())) {
            returnValue.add(getAnswer(request.getWrongAnswer4()));
        }

        return returnValue;

    }

    private AnswerBo getAnswer(String answer) {
        AnswerBo bo = new AnswerBo();
        bo.setAnswerText(answer);
        return bo;
    }
}
