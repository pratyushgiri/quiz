/*
 * Copyright (c) 2016
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.extropy.quiz.business.service;

import com.extropy.app.exception.ProcessingException;
import com.extropy.app.exception.ValidationException;
import com.extropy.quiz.business.pojo.QuestionRequest;
import com.extropy.quiz.business.transformer.OptimusPrime;
import com.extropy.quiz.common.annotation.QuizTransactional;
import com.extropy.quiz.common.bo.QuestionBo;
import com.extropy.quiz.common.bo.SubjectBo;
import com.extropy.quiz.dal.service.QuestionDbService;
import com.extropy.quiz.dal.service.SubjectDbService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;


/**
 * User: pratyushr
 * Date: 2/3/16
 * Time: 12:03 AM
 */
@Service
public class QuestionServiceImpl implements QuestionService {

    private static final org.slf4j.Logger logger =
                    LoggerFactory.getLogger(QuestionServiceImpl.class);

    @Autowired
    private OptimusPrime optimusPrime;

    @Autowired
    private QuestionDbService questionDbService;

    @Autowired
    private SubjectDbService subjectDbService;



    public QuestionBo addQuestion(QuestionRequest request) throws ProcessingException {

        validate(request);

        SubjectBo subjectBo = resolveSubject(request.getSubjectName());

        //TODO this is a hack
        if (optimusPrime == null) {
            optimusPrime = new OptimusPrime();
        }

        QuestionBo questionBo = optimusPrime.toQuestionBo(request);

        questionBo  = persistData (subjectBo, questionBo);

        return questionBo;

    }

    @QuizTransactional
    private QuestionBo persistData(SubjectBo subjectBo, QuestionBo questionBo) {

        logger.info("Persisting Question....");

        if (subjectBo.getId() == null){
            //Save Subject
            logger.info("Persisting Subject....");
            subjectBo = subjectDbService.saveSubject(subjectBo);
        }
        //save question
        questionBo.setSubjectId(subjectBo.getId());

        questionBo = questionDbService.saveQuestion(questionBo);

        logger.info("Saved the Question with ID:{}", questionBo.getId());
        return questionBo;
    }

    private SubjectBo resolveSubject(String subjectName) {


        SubjectBo bo = subjectDbService.getSubject(subjectName);

        if (bo == null) {
            bo = new SubjectBo();
            bo.setSubjectName(subjectName);
        }

        return bo;

    }

    private void validate(QuestionRequest request) throws ValidationException {

        try{
            Assert.notNull(request, "Question Request Cannot be null");
            Assert.isTrue(StringUtils.isNotBlank(request.getQuestionText()),"Question Text Can't be empty");
            Assert.isTrue(StringUtils.isNotBlank(request.getSubjectName()), "A Question must have a Subject");
            Assert.isTrue(StringUtils.isNotBlank(request.getCorrectAnswer()), "A Question must have a right answer");

            //Atleast one wrong answer must be there
            Assert.isTrue( StringUtils.isNotBlank(request.getWrongAnswer1())
                    || StringUtils.isNotBlank(request.getWrongAnswer2())
                    || StringUtils.isNotBlank(request.getWrongAnswer3())
                    || StringUtils.isNotBlank(request.getWrongAnswer4()) , "At least one wrong answer is needed"
            );

        }   catch(IllegalArgumentException ex) {
            logger.error("Validation Failed {}", ex);
            throw new ValidationException (ex.getMessage());
        }


    }


}
